// You can write a call and import your functions in this file.
//
// This file will be compiled into app.js
// Feel free with using ES6 here.

(($) => {
  // When DOM is ready
  $(() => {
    // Testing browser on lazyload native support
    if ('loading' in HTMLImageElement.prototype) {
      console.log('Supports lazyload');
    }
  });
})(jQuery);
